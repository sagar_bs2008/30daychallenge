package days;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

class Day2 {
	@BeforeSuite
	public void beforeSuite() {
		System.out.println("Day2.BeforeSuite");
	}
	@BeforeClass
	public void beforeClass() {
		System.out.println("Day2.beforeClass");
	}
	@BeforeMethod
	public void beforeMethod() {
		System.out.println("Day2.beforeMethod");
	}
	@Test
	public void Day2test1() {
		System.out.println("Day2.test1");
	}

	@Test
	public void Day2test2() {
		System.out.println("Day2.test2");
	}
	@Test
	public void Day2test3() {
		System.out.println("Day2.test3");
	}
	@Test
	public void Day2test4() {
		System.out.println("Day2.test4");
	}

	@AfterMethod
	public void afterMethod() {
		System.out.println("Day2.afterMethod");
	}

	@AfterClass
	public void afterClass() {
		System.out.println("Day2.afterClass");
	}

	@AfterSuite
	public void afterSuite() {
		System.out.println("Day2.afterSuite");
	}
}