package days;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;


public class Day4 {
	WebDriver driver;
	@BeforeSuite
	public void setup() {
		System.setProperty("webdriver.chrome.driver", "G:\\Educational\\SELFLEARN\\SELENIUM\\Driver\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://www.way2automation.com/demo.html");
	}
	
	@Test
	public void test1() {
		System.out.println(driver.getTitle());
		Assert.assertTrue(driver.getTitle().trim().equals("Welcome"), "Title does not match with Expected Result");
	}
	@Test
	public void test2() {
		System.out.println(driver.getTitle());
		Assert.assertTrue(driver.getTitle().trim().equals("Welcom"), "Title does not match with Expected Result");
	}
	@Test
	public void test3() {
		System.out.println(driver.getTitle());
		Assert.assertTrue(driver.getTitle().trim().equals("Welcome"), "Title does not match with Expected Result");
	}
	
	@AfterSuite
	public void teardown() {
		driver.quit();
	}
}
