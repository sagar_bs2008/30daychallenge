package days;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Day1 implements ITestListener {
WebDriver driver;
@BeforeSuite
public void beforeSuite() {
	//System.out.println("BeforeSuite");
	//System.setProperty("webdriver.gecko.driver", "G:\\Educational\\SELFLEARN\\SELENIUM\\Driver\\geckodriver.exe");
	System.setProperty("webdriver.chrome.driver", "G:\\Educational\\SELFLEARN\\SELENIUM\\Driver\\chromedriver.exe");
	driver=new ChromeDriver();
	driver.get("http://www.way2automation.com/demo.html");
}

@BeforeTest
public void beforeTest() {
	//System.out.println("beforeTest");
}
@BeforeGroups
public void beforeGroup() {
	//System.out.println("beforeGroup");
}

@BeforeClass
public void beforeClass() {
	//System.out.println("beforeClass");
}
@BeforeMethod
public void beforeMethod() {
	//System.out.println("beforeMethod");
}
@Test()
public void test1() {
	System.out.println(driver.getTitle());
}

@Test()
public void test2() {
	System.out.println("test2");
	Assert.assertTrue(driver.getTitle().equalsIgnoreCase("Hello"));
}
@Test (dependsOnMethods={"test2"})
public void test3() {
	System.out.println("test3");
}
@Test ()
public void test4() {
	System.out.println("test4");
}

@AfterMethod
public void afterMethod() {
	//System.out.println("AfterMethod");
}

@AfterClass
public void afterClass() {
	//System.out.println("afterClass");
}

@AfterTest
public void afterTest() {
	//System.out.println("afterTest");
}
@AfterGroups
public void AfterGroup() {
	//System.out.println("AfterGroup");
}

@AfterSuite
public void afterSuite() {
	//System.out.println("afterSuite");
	driver.quit();
}
@Override
public void onTestStart(ITestResult result) {
	System.out.println("Test start");
}
@Override
public void onTestSuccess(ITestResult result) {
	System.out.println("Test passed");
	
}
@Override
public void onTestFailure(ITestResult result) {
	System.out.println("Test Failed");
	File source_file = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	try {
		FileUtils.copyFile(source_file, new File("G:\\Educational\\SELFLEARN\\SELENIUM\\30daysPractice\\Switch30DayChallenge\\screenshot.png"));
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	 
	
}
@Override
public void onTestSkipped(ITestResult result) {
	System.out.println("Test Skipped");
}
@Override
public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
	// TODO Auto-generated method stub
	
}
@Override
public void onStart(ITestContext context) {
	System.out.println("Test Onstart");
	
}
@Override
public void onFinish(ITestContext context) {
	System.out.println("Test Finished");
	
}
}