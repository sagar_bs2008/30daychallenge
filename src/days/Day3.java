package days;

class Parent{
	Parent(int a){
		this.Parent1();
		System.out.println("Para Parent");
	}

	public Parent() {
		System.out.println("Default Parent");	
	}

	void Parent1(){
		System.out.println("Default Parent");
	}
	static {
		System.out.println("static in parent");
	}
	static void get() {
		System.out.println("static get in parent");
	}
	
}

public class Day3 extends Parent {
	Day3(){
		System.out.println("Default Child");
	}
	static void get() {
		System.out.println("static get in parent");
	}
	
	Day3(int a, int b){
		super();
		System.out.println("Para child");
	}
	static {
		System.out.println("static in child");
	}
	
	public static void main(String [] args) {
		new Day3(1,2);
		Parent.get();
		//Parent p1 = new Day3();
	}
	
}
